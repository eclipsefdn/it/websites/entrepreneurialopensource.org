# entrepreneurialopensource.org

The [entrepreneurialopensource.org](https://entrepreneurialopensource.org) website is generated with [Hugo](https://gohugo.io/documentation/).

The Entrepreneurial Open Source Podcast is where leaders and innovators who've made an impact in the open-source community while growing a profitable business, discuss the power and practical value of open source in business and entrepreneurship.

## Getting started

Install dependencies, build assets and start a webserver:

```bash
yarn
hugo server
```

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [entrepreneurialopensource.org](https://gitlab.eclipse.org/eclipsefdn/it/websites/entrepreneurialopensource.org) repository
2. Clone repository: `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/entrepreneurialopensource.org.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request


### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0


## Related projects

### [EclipseFdn/solstice-assets](https://gitlab.eclipse.org/eclipsefdn/it/webdev/solstice-assets)

Images, less and JavaScript files for the Eclipse Foundation look and feel.

### [EclipseFdn/hugo-solstice-theme](https://gitlab.eclipse.org/eclipsefdn/it/webdev/hugo-solstice-theme/)

Hugo theme of the Eclipse Foundation look and feel. 

## Bugs and feature requests

Have a bug or a feature request? Please search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.eclipse.org/eclipsefdn/it/websites/entrepreneurialopensource.org/-/issues).


## Author

**Christopher Guindon (Eclipse Foundation)**

- <https://twitter.com/chrisguindon>
- <https://github.com/chrisguindon>

## Trademarks

* Eclipse® is a Trademark of the Eclipse Foundation, Inc.
* Eclipse Foundation is a Trademark of the Eclipse Foundation, Inc.

## Copyright and license

Copyright 2018-2022 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [entrepreneurialopensource.org authors](https://gitlab.eclipse.org/eclipsefdn/it/websites/entrepreneurialopensource.org/-/graphs/main). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://gitlab.eclipse.org/eclipsefdn/it/websites/entrepreneurialopensource.org/-/raw/main/README.md).
